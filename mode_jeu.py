import numpy as np
import pygame
import time
from generate_universe import *
import sys
from display import *
from Regles_1 import *
from Jeu_1 import *
from affiche_victoire import *
from enregistrement_et_chargement import *



def mode_jeu(size, seed):

    """Fonction qui lance le programme en mode jeu"""

    # Ici on initialize les paramètres nécessaires pour le fonctionnement du programme

    # choix du seed
    universe = np.array(chargement(seed))
    size = len(universe)
    dico_eau = {(1,1) : [0,10]}

    universe = creation_coupelle(universe, size//2)
    nb_generation = 0

    #ParamÈtres internes pour le fonctionnement du code
    time_interval = 0.1
    bs = 800//size    # une cellule = bs*bs pixels
    LEFT = 1
    RIGHT = 3
    pause = False
    indicateur_victoire = False
    indicateur_defaite = False
    x_old, y_old = -1, -1

    # Paramètres liés au jeu: nombre de blocks dont dispose le joueur, et nombre de bolcks de l'eau à avoir dans la coupelle pous gagner
    Blocks_libres = 100
    Objectif_eau = 10


    # Initialisation de l'écran
    pygame.init()
    ecran = pygame.display.set_mode((800, 800))
    pygame.display.flip()

    while True:
        for event in pygame.event.get():

            # On quitte le programme
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit(0)

            elif event.type == pygame.KEYDOWN:
                # On pause si ce n'est pas déjà en pause, on annule la pause sinon
                if event.key == pygame.K_SPACE:
                    if pause == True:
                        pause = False
                    else:
                        pause = True

                # On accélere le temps
                elif event.key == pygame.K_RIGHT:
                    time_interval /= 2
                # On diminue le temps
                elif event.key == pygame.K_LEFT:
                    time_interval *= 2


            # Gère les actions à prendre lors d'un clique gauche

            if event.type == pygame.MOUSEBUTTONDOWN and event.button == LEFT:
                # Ajout / suprimme une cellule solide lors que le boutton gauche est préssé
                pressed_left = True
                pixel_x_initial, pixel_y_initial = pygame.mouse.get_pos()

                if universe[pixel_y_initial // bs, pixel_x_initial // bs] in [-1, -2]:
                    while pressed_left:

                        # calcule les coordonnées du pixel en haut à gauche de la cellule
                        pixel_x, pixel_y = pygame.mouse.get_pos()
                        x = bs*(pixel_x//bs)
                        y = bs*(pixel_y//bs)

                        # Si la cellule séçectionnée est un block solide, on l'efface
                        if (x_old != x or y_old != y) and universe[pixel_y//bs, pixel_x//bs] == -1:
                            #on colore en noir puis on inverse la valeur de la cellule
                            pygame.draw.rect(ecran, couleur_fond, (x, y, bs, bs))
                            universe[pixel_y//bs, pixel_x//bs] = 0
                            Blocks_libres = Blocks_libres + 1
                        for event in pygame.event.get():
                            if event.type == pygame.MOUSEBUTTONUP and event.button == LEFT:
                                pressed_left = False
                                break
                        pygame.display.flip()

                else:
                    while pressed_left:
                        # calcule les coordonnées du pixel en haut à gauche de la cellule
                        pixel_x, pixel_y = pygame.mouse.get_pos()
                        x = bs * (pixel_x // bs)
                        y = bs * (pixel_y // bs)

                        # Si la cellule séçectionnée est un block vide, on le remplace par un block solide
                        if (x_old != x or y_old != y) and Blocks_libres > 0 and universe[pixel_y // bs, pixel_x // bs] == 0:
                            # on colore en noir puis on inverse la valeur de la cellule
                            pygame.draw.rect(ecran, couleur_bloc_joueur, (x, y, bs, bs))
                            universe[pixel_y // bs, pixel_x // bs] = -1
                            Blocks_libres = Blocks_libres - 1
                        for event in pygame.event.get():
                            if event.type == pygame.MOUSEBUTTONUP and event.button == LEFT:
                                pressed_left = False
                                break
                        pygame.display.flip()

        if not pause:

            # Vérifie si les conditions de victoire ou défaite sont vraies
            if condition_defaite(dico_eau, nb_generation):
                indicateur_defaite = True
            if condition_victoire(universe, size//2, Objectif_eau):
                indicateur_victoire = True

            for x in range(size):
                for y in range(size):

                    # si c'est de l'eau, on peint la case en bleu
                    if universe[y,x] > 0:
                        pygame.draw.rect(ecran, color_cell(universe[y,x]), (x*bs, y*bs, bs, bs))

                    # si c'est du vide, on peint la case selon la couleur définit pour le vide
                    elif universe[y,x] == 0:
                        pygame.draw.rect(ecran, couleur_fond, (x*bs, y*bs, bs, bs))

                    # si c'est un solide placé pour le joeur, on le peint en rouge
                    elif universe[y,x] == -1:
                        pygame.draw.rect(ecran, couleur_bloc_joueur, (x*bs, y*bs, bs, bs))

                    # si c'est un solide, on le peint en blanc
                    elif universe[y,x] == -2:
                        pygame.draw.rect(ecran, couleur_bloc_fixe, (x*bs, y*bs, bs, bs))

            # Mise à jour d'univers
            nb_generation += 1
            universe = generation(universe, nb_generation, dico_eau)

        # Si les conditions de victoire / défaites sont vérifiées, on affiche le mesage de fin de jeu
        if indicateur_victoire == True:
            affiche_victoire(ecran)
        if indicateur_defaite == True and indicateur_victoire == False:
            affiche_defaite(ecran)

        # Mise à jour de l'écran
        pygame.display.flip()
        time.sleep(time_interval)

    pygame.quit()
