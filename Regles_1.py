import numpy as np



def chute(universe,i,j):

    '''Cette fonction simule la chute d'eau case par case'''

    if universe[i+1][j]>=0 and universe[i][j]>0 :
        base=universe[i][j]
        under=universe[i+1][j]

        if base+under<=1:
            universe[i][j]=0
            universe[i+1][j]=base+under

        else :
            universe[i][j]=base+under-1
            universe[i+1][j]=1

    return universe


def repartition(universe,i,j):

    '''Cette fonction répartit l'eau sur ces voisons'''

    if universe[i][j]>0.05:
        S=1
        old_water=universe[i][j]
        left=universe[i][j+1]
        right=universe[i][j-1]

        if left>=0 and left<old_water:
            S += 1

        if right>=0 and right<old_water:
            S=S+1
        #S répresente le nombre de voisons que possede la cellule (lui-meme inclus)


        #On reparti l'eau equitablement sur les voisins
        if left>=0 and left<old_water:
            universe[i][j+1]+=old_water/S

        if right>=0 and right<old_water:
            universe[i][j-1]+=old_water/S

        universe[i][j]=old_water/S

    return universe



def surpression(universe,i,j,max=1):

    '''On gere les cas de surpression (quantité d'eau dans une cellule supérieur au max)'''

    if universe[i][j] > max :

        excess_water=universe[i][j]-max

        if universe[i-1][j]>=0:

            universe[i-1][j]+=excess_water
            universe[i][j]=max

    return universe


def bords_nuls(universe):

    '''Rend la valeur des bords nul pour simuler la perte d'eau'''

    for j in range(len(universe[0])):
        if universe[-1][j]>0:
            universe[-1][j]=0

    for i in range(len(universe)):

        if universe[i][0]>0:
            universe[i][0]=0

        if universe[i][-1]>0:
            universe[i][-1]=0

    return universe



def creation_eau(universe,i,j,dico_eau,numero_generation):

    '''Crée une source d'eau pendant un certain nombre de générations'''

    if numero_generation<=dico_eau[(i,j)][1] and numero_generation>=dico_eau[(i,j)][0]:

        universe[i][j]+=1


    return universe



def generation(universe, numero_generation, dico_eau={}, max=1):

    '''On vide d'abord les bords'''

    bords_nuls(universe)

    #On applique ces regles a chaque cellule

    for i in range(len(universe)-2,-1,-1):
        #-2 car on ne considere pas le bord

        for j in range(1,len(universe[0])-1):
            universe=chute(universe,i,j)
            universe=repartition(universe,i,j)

        for j in range(len(universe[0])-2,0,-1):
            universe=repartition(universe,i,j)

            if (i,j) in dico_eau.keys():
                universe=creation_eau(universe,i,j,dico_eau,numero_generation)

    if numero_generation%2==0:

        for i in range(len(universe)-2,0,-1):

            for j in range(len(universe[0])):

                universe=surpression(universe,i,j,max)

    return universe





#A ce stade, le jeu fonctionne bien

def creation_plateforme_droit_vertical(i, j, size, universe, oscillation, nb_generation):

    if nb_generation%(2*oscillation)==0:

        for h in range(size):

            if universe[i-1][j+h]>=0 and universe[i-2][j+h]>=0:
                universe[i-2][j+h]+=universe[i-1][j+h]

            universe[i-1][j+h]=-3

    elif nb_generation%(2*oscillation)==oscillation:

        for h in range(size):

            if universe[i+1][j+h]>=0 and universe[i][j+h]>=0:

                universe[i][j+h]+=universe[i+1][j+h]

            universe[i][j+h]=-3

    return universe
