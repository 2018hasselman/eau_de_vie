import codecs, json
import numpy as np

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)



def save(nom_univers, univers):
    """enregistre l'univers appelé nom_univers dans le fichier"""
    #dictionnaire = chargement_dic()
    dictionnaire = chargement_dic()
    dictionnaire[nom_univers] = univers

    with open('Universes_database.txt', 'w') as file:
        json.dump(dictionnaire, file, cls=NumpyEncoder)




def chargement_dic():
    """retourne le dictionnaire entier contenu dans le fichier"""
    with open('Universes_database.txt', 'r') as file:
        dictionnaire = json.load(file)

    return dictionnaire


def chargement(nom_univers):
    dictionnaire = chargement_dic()
    return chargement_dic()[nom_univers]
