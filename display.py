import numpy as np
import pygame
import time
from generate_universe import *
from Regles_1 import *
import sys


def display_universe(universe):

    """Affiche l'univers à un instant figé"""

    size = len(universe)
    bs = 800//size

    # Initialisation de l'écran
    pygame.init()
    ecran = pygame.display.set_mode((800, 800))

    while True:
        for event in pygame.event.get():
            # on quitte le programme
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit(0)

        for x in range(size):
            for y in range(size):

                # si c'est de l'eau, on peint la case en bleu
                if universe[y,x] >= 0:
                    pygame.draw.rect(ecran, color_cell(universe[y,x]), (x*bs, y*bs, bs, bs))

                # si c'est un solide, on le peint en blanc
                elif universe[y,x] == -1:
                    pygame.draw.rect(ecran, (255,255,255), (x*bs, y*bs, bs, bs))

        # Mise à jour de l'écran
        pygame.display.flip()
        time.sleep(1)


def color_cell(niveau_eau):

    '''Prend le valeur du niveau de l'eau dans une cellule, et retourne la couleur RGB correspondent'''

    C = couleur_fond

    # trace des floats positifs (du valeur de l'eau d'une cellule) à des valeurs valables pour une couleur RGB
    if niveau_eau<=1:
        niveau_bleu = max(int(niveau_eau*200),60)
    elif niveau_eau<=3:
        niveau_bleu = int(niveau_eau*27.5 + 120 - 27.5)
    else:
        niveau_bleu = 255

    niveau_rouge = max(C[0]*(1-niveau_eau),0)

    niveau_vert = max(C[1]*(1-niveau_eau),0)

    couleur_cellule = (niveau_rouge , niveau_vert , niveau_bleu)

    # Rend l'affichage plus joli
    if couleur_cellule[2] < 15:
        return couleur_fond

    return couleur_cellule


# Configuration des couleurs utilisées dans l'affichage du jeu

couleur_fond = (78,46,67)
couleur_bloc_joueur = (200,0,0)
couleur_bloc_fixe = (255,255,255)
colour_text = (255,255,0)
