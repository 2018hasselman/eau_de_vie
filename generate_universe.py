import numpy as np
from display import *
from enregistrement_et_chargement import *
from Jeu_1 import *

def generate_universe(universe_size, seed = ""):

    """Génére un univers en fonction de l'input de l'utilisateur"""

    # Initialisation des paramètres nécessaires pour le code

    LEFT = 1    # Pour différencier les clicks droit et gauche sur pygame
    RIGHT = 3
    bs = 800//universe_size    # Homothétie de la taille de l'univers dans l'affichage
    continuer = True
    x_old, y_old = -1, -1

    # Dans le cas ou 'seed' est vide, on charge un univers initial vide
    if seed == "":
        universe = np.zeros((universe_size,universe_size), dtype = float)
    # Sinon, on charge l'univers correpondant au seed
    else:
        universe = np.array(chargement(seed))
        universe_size = len(universe)

    # Initialisation de l'écran
    pygame.init()
    ecran = pygame.display.set_mode((800, 800))

    # Crée la coupelle du jeu
    universe = creation_coupelle(universe, universe_size//2)

    # Loop principal du programme de génperation d'univers
    while continuer:

        for x in range(universe_size):
                for y in range(universe_size):

                    # si c'est de l'eau, on peint la case en bleu
                    if universe[y,x] > 0:
                        pygame.draw.rect(ecran, color_cell(universe[y,x]), (x*bs, y*bs, bs, bs))

                    # si c'est un solide, on le peint en blanc
                    elif universe[y,x] == -1:
                        pygame.draw.rect(ecran, couleur_bloc_joueur, (x*bs, y*bs, bs, bs))

                    # si c'est un block vide, on le peint avec la couleur définit pour le vide
                    elif universe[y,x] ==0:
                        pygame.draw.rect(ecran, couleur_fond, (x*bs, y*bs, bs, bs))

        for event in pygame.event.get():

            # Gère les actions à prendre lors d'un clique gauche

            if event.type == pygame.MOUSEBUTTONDOWN and event.button == LEFT:
                # Ajout / suprimme une cellule solide lors que le boutton gauche est préssé
                pressed_left = True
                pixel_x_initial, pixel_y_initial = pygame.mouse.get_pos()

                # Si la cellule séçectionnée est un block solide, on l'efface
                if universe[pixel_y_initial // bs, pixel_x_initial // bs] in [-1, -2]:
                    while pressed_left:

                        # calcule les coordonnées du pixel en haut à gauche de la cellule
                        pixel_x, pixel_y = pygame.mouse.get_pos()
                        x = bs*(pixel_x//bs)
                        y = bs*(pixel_y//bs)


                        if x_old != x or y_old != y:
                            #on colore en noir puis on change la valeur de la cellule
                            pygame.draw.rect(ecran, couleur_fond, (x, y, bs, bs))
                            universe[pixel_y//bs, pixel_x//bs] = 0
                        for event in pygame.event.get():
                            if event.type == pygame.MOUSEBUTTONUP and event.button == LEFT:
                                pressed_left = False
                                break
                        pygame.display.flip()

                # Si la cellule séçectionnée est un block vide, on le remplace avec un block solide
                else:
                    while pressed_left:

                        # calcule les coordonnées du pixel en haut à gauche de la cellule
                        pixel_x, pixel_y = pygame.mouse.get_pos()
                        x = bs * (pixel_x // bs)
                        y = bs * (pixel_y // bs)

                        if x_old != x or y_old != y:
                            # on colore en noir puis on change la valeur de la cellule
                            pygame.draw.rect(ecran, couleur_bloc_fixe, (x, y, bs, bs))
                            universe[pixel_y // bs, pixel_x // bs] = -2
                        for event in pygame.event.get():
                            if event.type == pygame.MOUSEBUTTONUP and event.button == LEFT:
                                pressed_left = False
                                break
                        pygame.display.flip()


            # Gère les actions à prendre lors d'un clique droit

            elif event.type == pygame.MOUSEBUTTONDOWN and event.button == RIGHT:

                    pressed_right = True
                    pixel_x_initial, pixel_y_initial = pygame.mouse.get_pos()

                    # Si la cellule séçectionnée est un block vide, on le remplace avec un block de l'eau
                    if universe[pixel_y_initial // bs, pixel_x_initial // bs] == 0:
                        while pressed_right:

                            # calcule les coordonnées du pixel en haut à gauche de la cellule
                            pixel_x, pixel_y = pygame.mouse.get_pos()
                            x = bs * (pixel_x // bs)
                            y = bs * (pixel_y // bs)

                            if x_old != x or y_old != y:
                                # on colore en noir puis on change la valeur de la cellule
                                pygame.draw.rect(ecran, color_cell(1), (x, y, bs, bs))
                                universe[pixel_y // bs, pixel_x // bs] = 1
                            for event in pygame.event.get():
                                if event.type == pygame.MOUSEBUTTONUP and event.button == RIGHT:
                                    pressed_right = False
                                    break
                            pygame.display.flip()
                    # Si la cellule séçectionnée est un block de l'eau, on le remplace avec un block vide
                    elif universe[pixel_y_initial // bs, pixel_x_initial // bs] > 0:
                        while pressed_right:

                            # calcule les coordonnées du pixel en haut à gauche de la cellule
                            pixel_x, pixel_y = pygame.mouse.get_pos()
                            x = bs * (pixel_x // bs)
                            y = bs * (pixel_y // bs)

                            if x_old != x or y_old != y:
                                # on colore en noir puis on change la valeur de la cellule
                                pygame.draw.rect(ecran, couleur_fond, (x, y, bs, bs))
                                universe[pixel_y // bs, pixel_x // bs] = 0
                            for event in pygame.event.get():
                                if event.type == pygame.MOUSEBUTTONUP and event.button == RIGHT:
                                    pressed_right = False
                                    break
                            pygame.display.flip()


            # on quitte le programme
            elif event.type == pygame.QUIT:
                continuer = False
                pygame.quit()
                sys.exit()

            elif event.type == pygame.KEYDOWN:

                # on presse entrer pour valider et finir le choix de l'univers
                if event.key ==pygame.K_KP_ENTER or event.key == pygame.K_RETURN:
                    continuer = False

                # si on tape ctrl+s, on enregistre
                elif event.key == pygame.K_s and pygame.key.get_mods() & pygame.KMOD_CTRL:
                    universe_name = input("Please enter the name of the universe to save: ")
                    save(universe_name, universe)
                    print("Universe saved")

        pygame.display.flip()


    pygame.quit()

    return universe

