import pygame


def affiche_victoire(ecran):

    ''' Affiche la mesage de victoire. Si victoire = vrai, la fonction returne aussi la valeur 1. Si il n'a pas
    de victoire, elle retourne 0.'''

    input_font = pygame.font.Font(None, 32)
    victoire_texte = 'Congratulations, you just poured water into a bucket!'


    victoire_txt = input_font.render(victoire_texte, True, (0, 0, 0))
    ecran.blit(victoire_txt, (10, 10))
    pygame.display.flip()


def affiche_defaite(ecran):

    ''' Affiche la mesage de victoire. Si victoire = vrai, la fonction returne aussi la valeur 1. Si il n'a pas
    de victoire, elle retourne 0.'''

    input_font = pygame.font.Font(None, 32)
    victoire_texte = 'Gameover, you lost :('


    victoire_txt = input_font.render(victoire_texte, True, (0, 0, 0))
    ecran.blit(victoire_txt, (10, 10))
    pygame.display.flip()
