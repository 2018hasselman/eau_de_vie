from Regles_1 import *
from pytest import *

Test2=[[-1,-1,-1],[0,0,0],[0,2,0],[-1,-1,-1]]


def test_chute():
    assert chute([[0,1,0],[0,0,0],[-1,-1,-1]],0,1)==[[0,0,0],[0,1,0],[-1,-1,-1]]

def test_repartition():
    assert repartition([[0,0,0],[0,3,0],[-1,-1,-1]],1,1) == [[0,0,0],[1,1,1],[-1,-1,-1]]

def test_surpression():
    assert surpression([[0],[2]],1,0) == [[1],[1]]

