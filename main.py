import numpy as np
import pygame
import time
from generate_universe import *
from Regles_1 import *
from display import *
from input_menu import *
from enregistrement_et_chargement import *
from mode_editeur import *
from mode_jeu import *




def main():

    """Fonction principale qui lance l'animation"""

    #récupération des données entrées

    pygame.display.set_caption('Eau de vie')

    choix = input_menu()


    seed = choix[3]
    if seed not in chargement_dic(): #si on rentre un seed inexistant, on construit un univers de taille demandée. Sinon, la taille est celle du seed
        size = int(choix[0])

    else:
        size = 80 #valeur par défaut

    if choix[1] == "1":
        mode_editeur(size, seed)

    elif choix[2] == "1":
        univers = mode_jeu(size, seed)







if __name__ == "__main__":
    main()
