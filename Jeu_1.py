def creation_coupelle(universe,j):
    '''Crée la coupelle ou l'utilisateur doit placer son eau'''
    if j+7 >len(universe[0]):
        print("Coupelle trop excentrée")
        return universe
    for i in range(len(universe)-1,len(universe)-9,-1):
        universe[i][j]=-2
        universe[i][j+7]=-2
    for h in range(j,j+8):
        universe[-1][h]=-2
    return universe



def condition_victoire(universe,j,Objectif_eau):
    '''Regarde si l'utilisateur a gagné'''
    somme_eau=0
    for i in range(len(universe)-1,len(universe)-9,-1):
        for h in range(j,j+8):
            if universe[i][h]>0:
                somme_eau+=universe[i][h]
    if somme_eau>=Objectif_eau:
        return True
    else:
        return False





Dico={(1,1):[0,10]}


def limitation_de_bloc(N,universe):
    '''Regarde si l'utilisateur a utilisé tous ses blocs ou pas (interdiction d'en placer plus le cas échéant)'''
    Somme_bloc_utilisateur=0
    for i in universe:
        for j in i:
            if j==-1:
                Somme_bloc_utilisateur+=1
    if Somme_bloc_utilisateur>=N:
        return False
    else:
        return True



def condition_defaite(dico_eau,nombre_generation):
    """Regarde si l'utilisateur a perdu"""
    N=max(dico_eau[(i,j)][1] for (i,j) in dico_eau.keys())
    if nombre_generation>=N+150:
        '''and not condition_victoire(universe,j,objectif_eau):'''
        return True
    else:
        return False



print(condition_defaite(Dico,251))
