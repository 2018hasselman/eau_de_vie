import pygame

def input_menu():
    pygame.init()
    ecran = pygame.display.set_mode((800, 400))

    # Configuration des paramètres de base pour l'affichage (coordonées, couleurs, tailles des rectangles)

    instruction_font = pygame.font.Font(None, 26)
    input_font = pygame.font.Font(None, 32)

    pos_zero = (10,10)
    label_size_1 = (300, 40)
    label_size_2 = (260, 40)
    input_size_1 = (300, 40)
    input_size_2 = (40, 40)
    instruction_size = (550, 20)
    separation = 10

    num_label = 6
    label = []
    label_text = ['Enter size of the universe: ', 'Select mode: ', 'Enter seed name: ', 'Editor mode', 'Game mode ', 'Confirm']
    label_position = [pos_zero,
                      (pos_zero[0], pos_zero[1]+label_size_1[1]+separation),
                      (pos_zero[0], pos_zero[1] + 3*label_size_1[1] + 3*separation),
                      (pos_zero[0] + label_size_1[0] + separation, pos_zero[1] + label_size_1[1] + separation),
                      (pos_zero[0] + label_size_1[0] + separation, pos_zero[1] + 2*label_size_1[1] + 2*separation),
                      (pos_zero[0], pos_zero[1] + 4*label_size_1[1] + 4*separation)]
    label_size = [label_size_1, label_size_1, label_size_1, label_size_2, label_size_2, label_size_1]
    label_color = (0, 0, 0)

    num_input = 4
    input_box = []
    input_box_position = [(pos_zero[0] + label_size_1[0] + separation, pos_zero[1]),
                          (pos_zero[0] + label_size_1[0] + label_size_2[0] + 2*separation, pos_zero[1] + input_size_1[1] + separation),
                          (pos_zero[0] + label_size_1[0] + label_size_2[0] + 2*separation, pos_zero[1] + 2*input_size_1[1] + 2*separation),
                          (pos_zero[0] + label_size_1[0] + separation, pos_zero[1] + 3*input_size_1[1] + 3*separation)]
    input_box_size = [input_size_1, input_size_2, input_size_2, input_size_1]
    input_box_color = (80, 80, 250)

    confirm_box_position = (pos_zero[0] + label_size_1[0] + separation, pos_zero[1] + 4*label_size_1[1] + 4*separation)
    toggle_indicator = 0

    num_instruction = 5
    instruction_position = [(pos_zero[0], pos_zero[1] + 5*label_size_1[1] + 6*separation),
                            (pos_zero[0], pos_zero[1] + 5*label_size_1[1] + 6*separation + instruction_size[1]),
                            (pos_zero[0], pos_zero[1] + 5*label_size_1[1] + 6*separation + 2*instruction_size[1]),
                            (pos_zero[0], pos_zero[1] + 5*label_size_1[1] + 6*separation + 3*instruction_size[1]),
                            (pos_zero[0], pos_zero[1] + 5*label_size_1[1] + 6*separation + 4*instruction_size[1])]

    instruction_text = ['Commandes: ', 'Souris gauche / droite: place de l eau / bloc solide', 'Espace: pause la simulation',
    'Flèches droite / gauche: augmenter / réduire vitesse de simulation', 'Crtl + S: en mode éditeur, sauvegarde l univers']

    input_box_text = []
    for i in range(0, num_input):
        input_box_text += ['']

    box_active = False
    box_indicator = 0
    continuer = True

    # Ici commence le programme

    while continuer:

        # Dessine les rectangles pour l'affichage des inputs et labels (possibilité de fonction 'dessine_rect')
        for i in range(0, num_label):
            label += [pygame.Rect(label_position[i], label_size[i])]
            pygame.draw.rect(ecran, label_color, label[i])

        for i in range(0, num_input):
            input_box += [pygame.Rect(input_box_position[i], input_box_size[i])]
            pygame.draw.rect(ecran, input_box_color, input_box[i], 3)

        confirm_box = pygame.Rect(confirm_box_position, input_size_2)
        pygame.draw.rect(ecran, (150, 255, 150), confirm_box)

        for i in range(0, num_instruction):
            instruction_box = pygame.Rect(instruction_position[i], instruction_size)
            pygame.draw.rect(ecran, label_color, instruction_box)

        # Definit les actions à prendre selon les bouttons pressé

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                for i in (0, 3):
                    if input_box[i].collidepoint(event.pos):
                        box_active = True
                        box_indicator = i
                        break
                    else:
                        box_active = False
                for i in (1, 2):
                    if input_box[i].collidepoint(event.pos):
                        toggle_indicator = i
                        input_box_text[i] = '1'
                if confirm_box.collidepoint(event.pos):
                    continuer = False

            if event.type == pygame.KEYDOWN:
                for i in [box_indicator]:
                    if box_active:
                        if event.key == pygame.K_BACKSPACE:
                            input_box_text[i] = input_box_text[i][:-1]
                        else:
                            input_box_text[i] += event.unicode

        # Affiche les bouttons de type 'toggle' et le texte sur les rectangles

        for i in range(0, num_label):
            label_txt = input_font.render(label_text[i], True, (200, 200, 200))
            ecran.blit(label_txt, (label_position[i][0]+10, label_position[i][1]+10))

        for i in range(0, num_instruction):
            instruction_txt = instruction_font.render(instruction_text[i], True, (200, 200, 200))
            ecran.blit(instruction_txt, (instruction_position[i][0]+2, instruction_position[i][1]+2))

        for i in (0, 3):
            input_txt = input_font.render(input_box_text[i], True, (200, 200, 200))
            pygame.draw.rect(ecran, (0,0,0), input_box[i])
            pygame.draw.rect(ecran, input_box_color, input_box[i], 3)
            ecran.blit(input_txt, (input_box[i].x + 10, input_box[i].y + 10))

        if toggle_indicator == 1:
            pygame.draw.rect(ecran, input_box_color, input_box[toggle_indicator])
            pygame.draw.rect(ecran, (0,0,0), input_box[2])
            input_box_text[2] = '0'
            toggle_indicator = 0
        if toggle_indicator == 2:
            pygame.draw.rect(ecran, input_box_color, input_box[toggle_indicator])
            pygame.draw.rect(ecran, (0,0,0), input_box[1])
            toggle_indicator = 0
            input_box_text[1] = '0'



        pygame.display.flip()

    pygame.quit()
    return input_box_text

