import numpy as np
import pygame
import time
from generate_universe import *
from Regles_1 import *
import sys
from display import *
from enregistrement_et_chargement import *
from Jeu_1 import *


def mode_editeur(size, seed):

    """Fonction qui s'occupe de l'édition des niveaux"""

    # Vérifie l'existence d'un seed, et crée l'univers approprié
    if seed not in chargement_dic():
        universe0 = generate_universe(size)
    else:
        universe0 = generate_universe(size, seed)

    # Création d'univers
    universe = np.array(universe0.copy())

    # Initialisation des paramètres nécessaires pour le fonctionnement du code
    size = len(universe)
    time_interval = 0.1
    nb_generation = 0
    bs = 800//size #une cellule = bs*bs pixels
    pause = False

    # Initialisation de l'écran
    pygame.init()
    ecran = pygame.display.set_mode((800, 800))
    pygame.display.flip()


    while True:
        for event in pygame.event.get():

            # on quitte le programme
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit(0)

            elif event.type == pygame.KEYDOWN:

                # on pause si ce n'est pas déjà en pause, on annule la pause sinon
                if event.key == pygame.K_SPACE:
                    if pause == True:
                        pause = False
                    else:
                        pause = True

                # on accélere / diminue le temps
                elif event.key == pygame.K_RIGHT:
                    time_interval /= 2
                elif event.key == pygame.K_LEFT:
                    time_interval *= 2

                # si on tape ctrl+s, on enregistre l'univers
                elif event.key == pygame.K_s and pygame.key.get_mods() & pygame.KMOD_CTRL:
                    universe_name = input("Please enter the name of the universe to save: ")
                    save(universe_name, universe)
                    print("Universe saved")

        if not pause:

            # colorage cellule par cellule
            for x in range(size):
                for y in range(size):

                    # si c'est de l'eau, on peint la case en bleu
                    if universe[y,x] > 0:
                        pygame.draw.rect(ecran, color_cell(universe[y,x]), (x*bs, y*bs, bs, bs))

                    # si c'est un solide, on le peint en blanc
                    elif universe[y,x] == -2:
                        pygame.draw.rect(ecran, couleur_bloc_fixe, (x*bs, y*bs, bs, bs))

                    # si c'est un solide placé pour le joeur, on le peint en rouge
                    elif universe[y,x] == -1:
                        pygame.draw.rect(ecran, couleur_bloc_joueur, (x*bs, y*bs, bs, bs))

                    # si c'est un block vide, on le peint avec la couleur définit pour le vide
                    elif universe[y,x] ==0:
                        pygame.draw.rect(ecran, couleur_fond, (x*bs, y*bs, bs, bs))

            # Mise à jour d'univers
            nb_generation += 1
            universe = generation(universe,nb_generation)

        # Mise à jour de l'écran
        pygame.display.flip()
        time.sleep(time_interval)

    pygame.quit()
